
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'submission_success' => 'Successfully submitted',
    'error' => 'Something went Wrong',
    'image_submission_success' => 'Image Successfully uploaded'
];