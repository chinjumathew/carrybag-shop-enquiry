<?php

namespace App\Http\Service\Api\V1;

use App\Http\Controllers\ApiController;
use Auth;
use Validator;
use App\Model\ShopEnquiry;
use App\Model\ShopImage;
use Illuminate\Support\Facades\Config;
use App\Http\Traits\ImageTrait;


class ShopEnquiryService extends ApiController
{
    use ImageTrait;

    public function shopEnquiry($request) {

        $validator = Validator::make($request->all(), [
            'sales_person' => 'required',
            'shop_type' => 'required',
            'shop_name' => 'required',
            'contact_person' => 'required',
            'mobile_number' => 'required',
            'shop_address' => 'required',
            'delivery_range' => 'required',
            'delivery_fee' => 'regex:/^\d+(\.\d{1,2})?$/',
            'status' => 'required',
            'shop_start_timing' => 'required|date_format:H:i',
            'shop_end_timing' => 'required|date_format:H:i',
        ]);
    
        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return $this->errorResponse(trans('api.error'), $errorMessage);
        }  
        
        $images_ids = $request->images_ids;

        $shop_enquiry = new ShopEnquiry();
        $shop_enquiry->sales_person = $request->sales_person;      
        $shop_enquiry->shop_type = $request->shop_type;
        $shop_enquiry->shop_name = $request->shop_name;
        $shop_enquiry->landline_number = $request->landline_number;
        $shop_enquiry->contact_person = $request->contact_person;
        $shop_enquiry->mobile_number = $request->mobile_number;
        $shop_enquiry->email = $request->email;
        $shop_enquiry->shop_address = $request->shop_address;
        $shop_enquiry->delivery_range = $request->delivery_range;
        $shop_enquiry->status = $request->status;
        $shop_enquiry->delivery_model = $request->delivery_model;
        $shop_enquiry->delivery_fee = $request->delivery_fee;
        $shop_enquiry->tax = $request->tax;
        $shop_enquiry->expected_delivery_time = $request->expected_delivery_time;
        $shop_enquiry->shop_start_timing = $request->shop_start_timing;
        $shop_enquiry->shop_end_timing = $request->shop_end_timing;
        $shop_enquiry->latitude = $request->latitude;
        $shop_enquiry->longitude = $request->longitude;
        $shop_enquiry->save();

        $images_ids = explode(',', $images_ids);
        
        if(!empty($images_ids)) {
            foreach($images_ids as $key => $value) {
                $image = ShopImage::where('id', $value)->first();
                if(!empty($image)) {
                    $image->shop_enquiry_id = $shop_enquiry->id;
                    $image->save();
                }
            }
        }
        return $this->successResponse(trans('api.submission_success'));

    }

    public function array_flatten($array) {
        $return = array();
        foreach ($array as $key => $value) {
            if (is_array($value)){
                $return = array_merge($return, array_flatten($value));
            } else {
                $return[$key] = $value;
            }
        }
    
        return $return;
    }

    public function imageStore($request) {

        $validator = Validator::make($request->all(), [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
           
        ]);
    
        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return $this->errorResponse(trans('api.error'), $errorMessage);
        } 
        
        $image = $request->file('image');
        $file = $image;
        $path = "shop_images/";
        $file_name = "shop_images_";
        
        $images = $this->imageUploads($file, $path, $file_name);
        
        $shop_image = new ShopImage();
        $shop_image->image = $images;
        $shop_image->save();
        $data['id'] = $shop_image->id;
        return $this->successResponse(trans('api.image_submission_success'), $data);

    }
    
}
