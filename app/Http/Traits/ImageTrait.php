<?php

// This is Image Traites

namespace App\Http\Traits;

use Intervention\Image\Facades\Image;
use Storage;
use File;

trait ImageTrait {

    /*
     * Image Upload to file
     */
    private function imageUploads($image, $path, $fileName) {
        $extension = $image->getClientOriginalExtension();
        $imageName = $fileName . rand(1, time()) . '.' . $image->getClientOriginalExtension();

        try {
            if (!env('CDN_ENABLED', false)) {
                
                $dir = public_path("uploads/" . $path);
                if (!File::isDirectory($dir)) {
                    File::makeDirectory($dir, 0775, true, true);
                }

                $imageRealPath = $image->getRealPath();
                $img = Image::make($imageRealPath);
                $img->save($dir . $imageName);
            } else {
                $dir = env('CDN_FILE_DIR', 'dev/folder-name/uploads/') . $path;
                $img = Image::make($image);
                Storage::disk('s3')->put($dir . $imageName, $img->stream()->detach(), 'public');
            }

            return $imageName;
        } catch (Exception $e) {
            return false;
        }
    }

    /*
     * Remove Image from file
     */
    private function imageRemove($file, $path) {
        if ($file) {
            $image_file = public_path('uploads/'.$path . '/' . $file);
            if (file_exists($image_file)) {
                unlink($image_file);
            }
        }
    }

    

    /*
     * Thumbnail image upload
     */

    private function thumbnailImage($image, $path, $fileName){
        $imageName = $fileName;
        try {
            if (!env('CDN_ENABLED', false)) {
                $dir = public_path("uploads/" . $path);
                if (!File::isDirectory($dir)) {
                    File::makeDirectory($dir, 0775, true, true);
                }
                file_put_contents('uploads/'.$path.'/'.$fileName, $image);
            } else {
                $dir = env('CDN_FILE_DIR', 'dev/folder-name/uploads/') . $path;
                $img = Image::make($image);
                Storage::disk('s3')->put($dir . $imageName, $img->stream()->detach(), 'public');
            }
            return $imageName;
        } catch (Exception $e) {
            return false;
        }
    }
      /*
     * upload Images from file
     */
    private function multiImage($image, $path, $fileName, $multiple = true) {
        $extension = $image->getClientOriginalExtension();
        $imageName = $fileName . rand(1, time()) . '.' . $image->getClientOriginalExtension();
        try {
            if (!env('CDN_ENABLED', false)) {
                $dir = public_path("uploads/" . $path);
                if (!File::isDirectory($dir)) {
                    File::makeDirectory($dir, 0775, true, true);
                    File::makeDirectory($dir . 'low/', 0775, true, true);
                    File::makeDirectory($dir . 'high/', 0775, true, true);
                    File::makeDirectory($dir . 'thumb/', 0775, true, true);
                }
                $imageRealPath = $image->getRealPath();

                $img = Image::make($imageRealPath);
                $img->save($dir . $imageName, 100);

                $height = $img->height();
                $width = $img->width();

                if ($multiple) {
                    $img->resize(300, null, function($constraint) {
                        $constraint->aspectRatio();
                    })->save($dir . 'low/' . $imageName);

                    $img->resize(700, null, function($constraint) {
                        $constraint->aspectRatio();
                    })->save($dir . 'high/' . $imageName);

                    $img->resize(100, 100, function($constraint) {
                    })->save($dir . 'thumb/' . $imageName);
                }
            } else {
                $dir = env('CDN_FILE_DIR','dev/dreaminn/uploads/') . $path;

                $img = Image::make($image);
                Storage::disk('s3')->put($dir . $imageName, $img->stream()->detach(), 'public');

                $height = $img->height();
                $width = $img->width();

                if ($multiple) {
                    $img->resize(300, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    Storage::disk('s3')->put($dir . '/low/' . $imageName, $img->stream()->detach(), 'public');

                    $img->resize(700, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    Storage::disk('s3')->put($dir . '/high/' . $imageName, $img->stream()->detach(), 'public');

                    $img->resize(100, 100, function ($constraint) {
                    });
                    Storage::disk('s3')->put($dir . '/thumb/' . $imageName, $img->stream()->detach(), 'public');
                }
            }

            return ['name' => $imageName, 'height' => $height, 'width' => $width, 'extn' => $extension];
        } catch (Exception $e) {
            return false;
        }
    }


}
