<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void`
     */
    public function __construct() {
        $this->middleware(['webApi']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function setApiLanguage($param) {

        App::setLocale($param);

        return $param;
    }

    public function setApiLanguageUserDriver($param, $type) {

        return true;
    }

    public function testLoad(){

      return $this->successResponse('');


    }

    public function successResponse($status, $data = '', $count = 0, $pageLimit = 0) {
        $page = null;
        $page['totalCount'] = $count;
        $page['limit'] = $pageLimit."";
        if (!empty($data)) {
            return response()->json([
                        "statusCode" => 1000,
                        "message" => $status,
                        "data" => $data,
                        "page" => $page,
            ]);
        } else {
            return response()->json([
                        "statusCode" => 1000,
                        "message" => $status
            ]);
        }
    }

    public function errorResponse($status, $errorMessage = null) {
        return response()->json([
                    "statusCode" => 2000,
                    "message" => $status,
                    "errorMessage" => $errorMessage
        ]);
    }

    public function sessionOutResponse($status) {
        return response()->json([
                    "statusCode" => 3000,
                    "message" => $status
        ]);
    }
}
