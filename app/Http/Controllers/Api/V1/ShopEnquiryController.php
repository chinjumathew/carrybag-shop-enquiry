<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Service\Api\V1\ShopEnquiryService;

class ShopEnquiryController extends Controller
{
    private $shop_enquiry_service;

    public function __construct(ShopEnquiryService $shop_enquiry_service) {

          $this->shop_enquiry_service = $shop_enquiry_service;
    }

    public function shopEnquiry(Request $request) {
        return $this->shop_enquiry_service->shopEnquiry($request); 
    }

    public function imageStore(Request $request) {
        return $this->shop_enquiry_service->imageStore($request); 
    }
}
