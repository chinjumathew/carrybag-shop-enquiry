<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopEnquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_enquiries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sales_person')->nullable();
            $table->string('shop_type')->nullable();
            $table->string('shop_name')->nullable();
            $table->string('landline_number')->nullable();
            $table->string('contact_person')->nullable();
            $table->string('mobile_number')->nullable();
            $table->string('email')->nullable();
            $table->text('shop_address')->nullable();
            $table->string('delivery_range')->nullable();
            $table->string('status')->nullable();
            $table->string('delivery_model')->nullable();
            $table->decimal('delivery_fee',15,8)->default(0)->nullable();
            $table->string('tax')->nullable();
            $table->time('expected_delivery_time')->nullable();
            $table->time('shop_start_timing')->nullable();
            $table->time('shop_end_timing')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_enquiries');
    }
}
